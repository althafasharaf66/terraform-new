provider "aws" {}
resource "aws_vpc" "main" {
    cidr_block = "10.0.0.0/16"
    tags = {
        Name : "development",
    }
}
variable "subnet_cidr_block" {
    description = "subnet cidr block"
    type = list(string)
}
variable zone {
    description = "available zone"
    type = list(string)
}
variable "avail_zone" {}
resource "aws_subnet" "subnet-1" {
    vpc_id = aws_vpc.main.id
    cidr_block = var.subnet_cidr_block[0]
    availability_zone = var.avail_zone
    tags = {
        Name : "subnet_1"
    }
}
data "aws_vpc" "existing_vpc" {
    default = true
}
resource "aws_subnet" "subnet-2" {
    vpc_id = data.aws_vpc.existing_vpc.id
    cidr_block = "172.31.48.0/20"
    availability_zone = var.zone[0]
    tags = {
        Name : "subnet_2"
    }
}

output "detail_subnet" {
    value = aws_subnet.subnet-1.id
}